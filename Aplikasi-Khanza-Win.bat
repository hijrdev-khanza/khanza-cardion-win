@echo off
start "Cloudflared Tunnel" "C:\Program Files (x86)\cloudflared\cloudflared.exe" access tcp --hostname tcp.site.com --url localhost:15001

echo Waiting for Cloudflare tunnel to start...
timeout /t 5 /nobreak >nul

java -jar SIMRSKhanza.jar

pause

